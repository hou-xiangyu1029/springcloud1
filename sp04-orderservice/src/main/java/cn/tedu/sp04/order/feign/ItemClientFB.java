package cn.tedu.sp04.order.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class ItemClientFB implements ItemClient{
    @Override
    public JsonResult<List<Item>> getItems(String orderId) {
        if (Math.random()<0.5){
            List<Item> items = new ArrayList<>();
            items.add(new Item(1, "缓存商品1",1));
            items.add(new Item(2, "缓存商品2",2));
            items.add(new Item(3, "缓存商品3",3));
            items.add(new Item(4, "缓存商品4",4));
            items.add(new Item(5, "缓存商品5",5));
            items.add(new Item(6, "缓存商品6",6));
            return JsonResult.ok().data(items);
        }
        return JsonResult.err().msg("获取订单的商品列表失败");
    }

    @Override
    public JsonResult decreaseNumber(List<Item> items) {
        return JsonResult.err().msg("减少库存失败");
    }
}
