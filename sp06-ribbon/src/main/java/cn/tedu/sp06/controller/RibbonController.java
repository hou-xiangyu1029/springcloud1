package cn.tedu.sp06.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.pojo.Order;
import cn.tedu.sp01.pojo.User;
import cn.tedu.web.util.JsonResult;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@Slf4j
public class RibbonController {
    @Autowired
    private RestTemplate rt;


    @GetMapping("item-service/{orderId}")
    @HystrixCommand(fallbackMethod = "getItemsFB")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId){
        //{1}RestTemplate 自己定义的一种占位符,,用orderId填充占位符
        /*注册中心的注册表
        * item-service ---- localhost：8001，localhost：8002*/
        return rt.getForObject("http://item-service/{1}", JsonResult.class,orderId);
    }

    @PostMapping("/item-service/decreaseNumber")
    @HystrixCommand(fallbackMethod = "decreaseNumberFB")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items){
        return rt.postForObject("http://item-service/decreaseNumber", items, JsonResult.class);

    }

    @GetMapping("/user-service/{userId}")
    @HystrixCommand(fallbackMethod = "getUserFB")
    public JsonResult<User> getUser(@PathVariable Integer userId){
        return rt.getForObject("http://user-service/{1}", JsonResult.class,userId);

    }
    @GetMapping("/user-service/{userId}/score")
    @HystrixCommand(fallbackMethod = "addScoreFB")
    public JsonResult<?> addScore(@PathVariable Integer userId,@RequestParam Integer score){
        return rt.getForObject("http://user-service/{1}/score?score={2}", JsonResult.class,userId,score);
    }

    @GetMapping("/order-service/{orderId}")
    @HystrixCommand(fallbackMethod = "getOrderFB")
    public JsonResult<Order> getOrder(@PathVariable String orderId){
        return rt.getForObject("http://order-service/{1}", JsonResult.class,orderId);
    }
    @HystrixCommand(fallbackMethod = "addOrderFB")
    @GetMapping("/order-service/")
    public JsonResult<?> addOrder(){
        return rt.getForObject("http://order-service/",JsonResult.class);
    }



////////////////////////////////////////////////////


    public JsonResult<List<Item>> getItemsFB(@PathVariable String orderId){
        return JsonResult.err().msg("获取订单的商品列表失败");
    }


    public JsonResult<?> decreaseNumberFB(@RequestBody List<Item> items){
        return JsonResult.err().msg("减少商品数量失败");

    }


    public JsonResult<User> getUserFB(@PathVariable Integer userId){
        return JsonResult.err().msg("获取用户列表失败");

    }

    public JsonResult<?> addScoreFB(@PathVariable Integer userId,@RequestParam Integer score){
        return JsonResult.err().msg("增加积分失败");

    }


    public JsonResult<Order> getOrderFB(@PathVariable String orderId){
        return JsonResult.err().msg("获取订单列表失败");

    }

    public JsonResult<?> addOrderFB(){
        return JsonResult.err().msg("添加订单失败");

    }




}
